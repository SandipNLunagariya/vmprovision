resource "google_compute_instance" "myfirstdefaultvm" {
  project      = "bldc-integration-with-gcp"
  name         = "${var.instance_name}"
  machine_type = "${var.instance_type}"
  zone         = "${var.instance_zone}"

  boot_disk {
    initialize_params {
      image = "rhel-cloud/rhel-6"
    }
  }

  network_interface {
    network = "default"
    access_config {
    }
  }
}