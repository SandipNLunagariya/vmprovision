provider "google" {
    credentials = "${file("bldc-integration-with-gcp-b7bc851f8734.json")}"
    project      = "bldc-integration-with-gcp"
}

  terraform {
  	backend "gcs" {
  	bucket  = "tf-bucket2"
	prefix  = "terraform/state"
	credentials = "bldc-integration-with-gcp-b7bc851f8734.json"
 	}
  }