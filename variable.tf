variable instance_name {
	description = "Default, vm name."
  	default     = "terraform-default"
}

variable instance_type {
	description = "Default, vm type."
	default = "n1-standard-1"
}

variable instance_zone {
	description = "Default, vm zone."
	default = "us-central1-a"
}